variable "ec2_ami" {
  type = map

  default = {
    us-east-1 = "ami-08a0d1e16fc3f61ea"
  }
}

# Creating a Variable for region

variable "region" {
  default = "us-east-1"
}


# Creating a Variable for instance_type
variable "instance_type" {    
  type = string
}